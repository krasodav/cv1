package cz.fit.cvut.tjv.service;

import cz.fit.cvut.tjv.Level;

public class SlackService implements LoggerService {

    @Override
    public void log(String msg, Level level) {
        System.out.println("[Slack]"+ level.toString() + msg);
    }
}
