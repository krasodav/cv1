package cz.fit.cvut.tjv.service;

import cz.fit.cvut.tjv.Level;

public class MailService implements LoggerService {

    @Override
    public void log(String msg, Level level) {
        System.out.println("[E-mail]"+ level.toString() + msg);
    }
}
