package cz.fit.cvut.tjv.service;

import cz.fit.cvut.tjv.Level;

public interface LoggerService {

    void log(String msg, Level level);

}
