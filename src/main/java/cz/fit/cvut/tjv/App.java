package cz.fit.cvut.tjv;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    public static void main(String[] args) {
        //ApplicationContext context = new ClassPathXmlApplicationContext("applicationConfig.xml");
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        Logger logger = (Logger) context.getBean("logger");
        logger.log("Spring",Level.DEBUG);

    }
}
