package cz.fit.cvut.tjv;

import cz.fit.cvut.tjv.service.LoggerService;
import cz.fit.cvut.tjv.service.MailService;

public class Logger {

    private LoggerService service;

    public Logger(LoggerService service){
        this.service = service;
    }
    public void log( String msg, Level level){
        service.log(msg,level);
    }

    public LoggerService getService() {
        return service;
    }

    public void setService(LoggerService service) {
        this.service = service;
    }
}
