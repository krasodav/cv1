package cz.fit.cvut.tjv;

import cz.fit.cvut.tjv.service.LoggerService;
import cz.fit.cvut.tjv.service.SlackService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public Logger logger(){
        return new Logger(loggerService());
    }

    @Bean
    public LoggerService loggerService(){
        return new SlackService();
    }
}
