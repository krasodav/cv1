package cz.fit.cvut.tjv;

public enum Level {
    DEBUG("DEBUG"),
    WARNING("WARN"),
    INFO("INFO"),
    ERROR("ERROR");

    private String name;

    private Level(String name){
        this.name = name;
    }

    public String toString(){
        return "[" + name + "]: ";
    }
}
